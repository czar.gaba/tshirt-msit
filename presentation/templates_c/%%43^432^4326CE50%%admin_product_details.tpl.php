<?php /* Smarty version 2.6.25-dev, created on 2017-10-07 05:46:26
         compiled from admin_product_details.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'load_presentation_object', 'admin_product_details.tpl', 2, false),array('function', 'html_options', 'admin_product_details.tpl', 61, false),)), $this); ?>
<?php echo smarty_function_load_presentation_object(array('filename' => 'admin_product_details','assign' => 'obj'), $this);?>

<form enctype="multipart/form-data" method="post"
 action="<?php echo $this->_tpl_vars['obj']->mLinkToProductDetailsAdmin; ?>
">
  <h3>
    Editing product: ID #<?php echo $this->_tpl_vars['obj']->mProduct['product_id']; ?>
 &mdash;
    <?php echo $this->_tpl_vars['obj']->mProduct['name']; ?>
 [
    <a href="<?php echo $this->_tpl_vars['obj']->mLinkToCategoryProductsAdmin; ?>
">
      back to products ...</a> ]
  </h3>
  <?php if ($this->_tpl_vars['obj']->mErrorMessage): ?><p class="error"><?php echo $this->_tpl_vars['obj']->mErrorMessage; ?>
</p><?php endif; ?>
  <table class="borderless-table">
    <tbody>
      <tr>
        <td valign="top">
          <p class="bold-text">
            Product name:
          </p>
          <p>
            <input type="text" name="name"
            value="<?php echo $this->_tpl_vars['obj']->mProduct['name']; ?>
" size="30" />
          </p>
          <p class="bold-text">
            Product description:
          </p>
          <p>
            <?php echo '<textarea name="description" rows="3" cols="60">'; ?><?php echo $this->_tpl_vars['obj']->mProduct['description']; ?><?php echo '</textarea>'; ?>

          </p>
          <p class="bold-text">
            Product price:
          </p>
          <p>
            <input type="text" name="price"
             value="<?php echo $this->_tpl_vars['obj']->mProduct['price']; ?>
" size="5" />
          </p>
          <p class="bold-text">
            Product discounted price:
          </p>
          <p>
            <input type="text" name="discounted_price"
             value="<?php echo $this->_tpl_vars['obj']->mProduct['discounted_price']; ?>
" size="5" />
          </p>
          <p>
            <input type="submit" name="UpdateProductInfo"
             value="Update info" />
          </p>
        </td>
        <td valign="top">
          <p>
            <font class="bold-text">Product belongs to these categories:</font>
            <?php echo $this->_tpl_vars['obj']->mProductCategoriesString; ?>

          </p>
          <p class="bold-text">
            Remove this product from:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'TargetCategoryIdRemove','options' => $this->_tpl_vars['obj']->mRemoveFromCategories), $this);?>

            <input type="submit" name="RemoveFromCategory" value="Remove"
             <?php if ($this->_tpl_vars['obj']->mRemoveFromCategoryButtonDisabled): ?>
             disabled="disabled" <?php endif; ?>/>
          </p>
          <p class="bold-text">
            Assign product to this category:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'TargetCategoryIdAssign','options' => $this->_tpl_vars['obj']->mAssignOrMoveTo), $this);?>

            <input type="submit" name="Assign" value="Assign" />
          </p>
          <p class="bold-text">
            Move product to this category:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'TargetCategoryIdMove','options' => $this->_tpl_vars['obj']->mAssignOrMoveTo), $this);?>

            <input type="submit" name="Move" value="Move" />
            <input type="submit" name="RemoveFromCatalog"
             value="Remove product from catalog"
             <?php if (! $this->_tpl_vars['obj']->mRemoveFromCategoryButtonDisabled): ?>
             disabled="disabled" <?php endif; ?>/>
          </p>
          <?php if ($this->_tpl_vars['obj']->mProductAttributes): ?>
          <p class="bold-text">
            Product attributes:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'TargetAttributeValueIdRemove','options' => $this->_tpl_vars['obj']->mProductAttributes), $this);?>

            <input type="submit" name="RemoveAttributeValue"
             value="Remove" />
          </p>
          <?php endif; ?>
          <?php if ($this->_tpl_vars['obj']->mCatalogAttributes): ?>
          <p class="bold-text">
            Assign attribute to product:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'TargetAttributeValueIdAssign','options' => $this->_tpl_vars['obj']->mCatalogAttributes), $this);?>

            <input type="submit" name="AssignAttributeValue"
             value="Assign" />
          </p>
          <?php endif; ?>
          <p class="bold-text">
            Set display option for this product:
          </p>
          <p>
            <?php echo smarty_function_html_options(array('name' => 'ProductDisplay','options' => $this->_tpl_vars['obj']->mProductDisplayOptions,'selected' => $this->_tpl_vars['obj']->mProduct['display']), $this);?>

            <input type="submit" name="SetProductDisplayOption" value="Set" />
          </p>
        </td>
      </tr>
    </tbody>
  </table>
  <p>
    <font class="bold-text">Image name:</font> <?php echo $this->_tpl_vars['obj']->mProduct['image']; ?>

    <input name="ImageUpload" type="file" value="Upload" />
    <input type="submit" name="Upload" value="Upload" />
  </p>
  <?php if ($this->_tpl_vars['obj']->mProduct['image']): ?>
  <p>
    <img src="product_images/<?php echo $this->_tpl_vars['obj']->mProduct['image']; ?>
"
     border="0" alt="<?php echo $this->_tpl_vars['obj']->mProduct['name']; ?>
 image" />
  </p>
  <?php endif; ?>
  <p>
    <font class="bold-text">Image 2 name:</font> <?php echo $this->_tpl_vars['obj']->mProduct['image_2']; ?>

    <input name="Image2Upload" type="file" value="Upload" />
    <input type="submit" name="Upload" value="Upload" />
  </p>
  <?php if ($this->_tpl_vars['obj']->mProduct['image_2']): ?>
  <p>
    <img src="product_images/<?php echo $this->_tpl_vars['obj']->mProduct['image_2']; ?>
"
     border="0" alt="<?php echo $this->_tpl_vars['obj']->mProduct['name']; ?>
 image 2" />
  </p>
  <?php endif; ?>
  <p>
    <font class="bold-text">Thumbnail name:</font> <?php echo $this->_tpl_vars['obj']->mProduct['thumbnail']; ?>

    <input name="ThumbnailUpload" type="file" value="Upload" />
    <input type="submit" name="Upload" value="Upload" />
  </p>
  <?php if ($this->_tpl_vars['obj']->mProduct['thumbnail']): ?>
  <p>
    <img src="product_images/<?php echo $this->_tpl_vars['obj']->mProduct['thumbnail']; ?>
"
     border="0" alt="<?php echo $this->_tpl_vars['obj']->mProduct['name']; ?>
 thumbnail" />
  </p>
  <?php endif; ?>
</form>